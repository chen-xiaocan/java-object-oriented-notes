

# 笔记

```java
//  % 取余 获取的是两个数据做除法的余数
//  +符号做连接符 能算则算，不能算就在一起
//  ++ 自增 变量自身的值加1； -- 自减 变量自身的值减1；
//  ++、-- 如果不是单独使用（如在表达式中，或同时有其他操作），放在变量前后会存在明显区别
//  放在变量前面，先对变量进行+1、-1，再拿变量进行运算
			int a = 10;
			int rs = ++a; //rs=11,a=11
//  放在变量后面，先拿变量的值进行运算，再对变量的值进行+1、-1,
			int b =10;
			int rs = b++;//rs=10,b=11  
```





# 作业

```java
package zy1;

import java.util.Calendar;
import java.util.Scanner;
//        # 巩固题
//
//## 1、判断5的倍数
//从键盘输入一个整数，判断它是否是5的倍数
public class zy1 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("输入一个整数：");
        int a = sc1.nextInt();
        if(a%5==0){
            System.out.println(a+"是5的倍数");
        }else{
            System.out.println(a+"不是5的倍数");
        }
//
//## 2、判断字符类型
// 从键盘输入一个字符，判断它是字母还是数字，还是其他字符
        Scanner sc2 = new Scanner(System.in);
        System.out.println("输入一个字符：");
        char b = sc2.next().charAt(0);
        if(b>='a' && b<='z' || b>='A' && b<='Z'){
            System.out.println(b+"是字母");
        }else if(b>='0' && b<='9'){
            System.out.println(b+"是数字");
        }else{
            System.out.println(b+"是其他字符");
        }
//
//## 3、计算折扣后金额
//        从键盘输入订单总价格totalPrice（总价格必须>=0），根据优惠政策计算打折后的总价格。
//        编写步骤：
//        1. 判断当`totalPrice >=500` ,discount赋值为0.8
//        2. 判断当`totalPrice >=400` 且`<500`时,discount赋值为0.85
//        3. 判断当`totalPrice >=300` 且`<400`时,discount赋值为0.9
//        4. 判断当`totalPrice >=200` 且`<300`时,discount赋值为0.95
//        5. 判断当`totalPrice >=0` 且`<200`时,不打折，即discount赋值为1
//        6. 判断当`totalPrice<0`时，显示输入有误
//        7. 输出结果
        Scanner sc3 = new Scanner(System.in);
        System.out.println("输入一个整数：");
        int totalPrice = sc3.nextInt();
        if(totalPrice>=500){
            double discount = 0.8;
            System.out.println("折扣为:"+discount);
        }else if(totalPrice>=400 && totalPrice<500){
            double discount = 0.85;
            System.out.println("折扣为:"+discount);
        }else if(totalPrice>=300 && totalPrice<400){
            double discount = 0.9;
            System.out.println("折扣为:"+discount);
        }else if(totalPrice>=200 && totalPrice<300){
            double discount = 0.95;
            System.out.println("折扣为:"+discount);
        }else if(totalPrice>=0 && totalPrice<200){
            double discount = 1;
            System.out.println("不打折,折扣为:"+discount);
        }else if(totalPrice<0){
            System.out.println("输入有误");
        }

//
//## 4、输出月份对应的英语单词
//
//        从键盘输入月份值，输出对应的英语单词
        Scanner sc4 = new Scanner(System.in);
        System.out.println("输入一个月份：");
        int d = sc4.nextInt();
        if(d==1){
            System.out.println("January");
        }else if(d==2){
            System.out.println("February");
        }else if(d==3){
            System.out.println("March");
        }else if(d==4){
            System.out.println("April");
        }else if(d==5){
            System.out.println("May");
        }else if(d==6){
            System.out.println("June");
        }else if(d==7){
            System.out.println("July");
        }else if(d==8){
            System.out.println("August");
        }else if(d==9){
            System.out.println("September");
        }else if(d==10){
            System.out.println("October");
        }else if(d==11){
            System.out.println("November");
        }else if(d==12){
            System.out.println("December");
        }

//
//## 5、计算今天是星期几
//定义变量week赋值为上一年12月31日的星期值（可以通过查询日历获取），定义变量year、month、day，分别赋值今天日期年、月、日值。计算今天是星期几。
//
        Calendar cal = Calendar.getInstance();
        int week = cal.get(Calendar.DAY_OF_WEEK);
        switch (week) {
            case 1:
                System.out.println("今天是星期日");
                break;
            case 2:
                System.out.println("今天是星期一");
                break;
            case 3:
                System.out.println("今天是星期二");
                break;
            case 4:
                System.out.println("今天是星期三");
                break;
            case 5:
                System.out.println("今天是星期四");
                break;
            case 6:
                System.out.println("今天是星期五");
                break;
            case 7:
                System.out.println("今天是星期六");
                break;
        }
# 拔高题

## 1、判断年、月、日是否合法

从键盘输入年、月、日，要求年份必须是正整数，月份范围是[1,12]，日期也必须在本月总天数范围内，如果输入正确，输出“年-月-日”结果，否则提示输入错误。

## 2、判断打鱼还是晒网

从键盘输入年、月、日，假设从这一年的1月1日开始执行三天打鱼两天晒网，那么你输入的这一天是在打鱼还是晒网。

## 3、判断星座

声明变量month和day，用来存储出生的月份和日期，判断属于什么星座，各个星座的日期范围如下：

![1558000604568](03_Java流程控制语句结构作业图片/1558000604568.png)

# 简答题

1、switch是否能作用在byte上，是否能作用在long上，是否能作用在String上？





```

